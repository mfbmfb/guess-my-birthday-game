from random import randint

name = (input("Hi! What is your name?"))

# guess 1

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 1 :", name, "Were you born in ",
     month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# guess 2

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 2 :", name, "Were you born in ",
     month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# guess 3

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 3 :", name, "Were you born in ",
     month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

#guess 4

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 4 :", name, "Were you born in ",
     month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# guess 5

month_number = randint(1, 12)
year_number = randint(1924, 2004)

print("Guess 5 :", name, "Were you born in ",
     month_number, "/", year_number, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("I have other things to do. Good bye")

